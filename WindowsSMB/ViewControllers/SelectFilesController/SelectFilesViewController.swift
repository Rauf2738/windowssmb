//
//  SelectFilesViewController.swift
//  WindowsSMB
//
//  Created by JustDoIt on 09.10.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit
import SMBClient

class SelectFilesViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var fileServer: SMBFileServer!
    var files: [SMBShare]! = [SMBShare]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAllFilesFromPC()
    }
    
    func getAllFilesFromPC() {
        fileServer.listShares { (files, error) in
            guard error == nil else {
                return
            }
            self.files = files
            self.collectionView.reloadData()
        }
    }
    
}

extension SelectFilesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return files.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SelectPCCollectionViewCell
        let file = files[indexPath.row]
        let pcTitle = "Name = \(file.name)"
        cell.configureWith(title: pcTitle)
        return cell
    }
    
}
