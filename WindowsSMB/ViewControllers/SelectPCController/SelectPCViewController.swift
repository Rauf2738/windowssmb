//
//  SelectPCViewController.swift
//  WindowsSMB
//
//  Created by JustDoIt on 09.10.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit
import SMBClient

class SelectPCViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var fileServer: SMBFileServer!
    var devices: [SMBDevice]! = [SMBDevice]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAllPCInWorkGroup()
    }
    
    func getAllPCInWorkGroup() {
        SMBDiscovery.sharedInstance()?.start(of: .any, added: { (device) in
            self.devices.append(device)
            self.collectionView.reloadData()
        }, removed: { (device) in
            print("Removed \(device)")
        })
    }
    
    private func alertWithAuthorization() {
        let alertController = UIAlertController(title: nil, message: "Login/Signup", preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Login"
            textField.keyboardType = .emailAddress
        }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Password"
            textField.isSecureTextEntry = true
        }
        
        let loginAction = UIAlertAction(title: "Login", style: .default) { (_) in
            let loginField = alertController.textFields![0]
            let passwordField = alertController.textFields![1]
            self.fileServerConnect(login: loginField.text ?? "", pass: passwordField.text ?? "")
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(loginAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func fileServerConnect(login: String, pass: String) {
        guard fileServer != nil else {
            return
        }
        
        fileServer.connect(asUser: login, password: pass, domain: "192.168.88.28", completion: { (guest, error) in
            if error != nil {
                print("Unable to connect:\(error?.localizedDescription ?? "")")
                return
            } else if (guest) {
                let selectFilesViewController = SelectFilesViewController()
                selectFilesViewController.fileServer = self.fileServer
                self.present(selectFilesViewController, animated: true, completion: nil)
            } else {
                let cancelAlertController = UIAlertController.init(title: "", message: "Erroe", preferredStyle: .alert)
                cancelAlertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (_) in
                    self.view.endEditing(true)
                }))
            }
        })
    }
}
    

extension SelectPCViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return devices.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SelectPCCollectionViewCell
        let device = devices[indexPath.row]
        let pcTitle = "Group = \(device.group ?? "") \n Host = \(device.host ?? "") \n Name = \(device.netbiosName  ?? "") \n Type = \(device.type)"
        cell.configureWith(title: pcTitle)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let device = devices[indexPath.row]
        fileServer = SMBFileServer.init(host: device.host, netbiosName: device.netbiosName, group: device.group)
        alertWithAuthorization()
    }
    
}
