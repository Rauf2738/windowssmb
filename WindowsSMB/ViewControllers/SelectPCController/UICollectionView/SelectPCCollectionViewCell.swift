//
//  SelectPCCollectionViewCell.swift
//  WindowsSMB
//
//  Created by JustDoIt on 09.10.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class SelectPCCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    func configureWith(title: String) {
        titleLabel.text = title
    }
}
